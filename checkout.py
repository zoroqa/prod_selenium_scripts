# -*- coding: utf-8 -*-
from selenium import webdriver
import  xmlrunner
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class Checkout(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_checkout(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("power")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_link_text("ZORO 50 ft. 12/3 SJTW Lighted Extension Cord YL/BL").click()
        for i in range(60):
            try:
                if self.is_element_present(By.ID, "add-to-cart-btn-G1385984"): break
            except:
                pass
            time.sleep(1)
        else:
            self.fail("time out")
        driver.find_element_by_id("add-to-cart-btn-G1385984").click()
        driver.find_element_by_id("zoro-logo").click()
        driver.find_element_by_css_selector("div.pull-left.zoro-controls-cart > a").click()
        for i in range(60):
            try:
                if self.is_element_present(By.ID, "btn-proceed-checkout"): break
            except:
                pass
            time.sleep(1)
        else:
            self.fail("time out")
        driver.find_element_by_id("btn-proceed-checkout").click()
        driver.find_element_by_id("login-radio-new").click()
        for i in range(60):
            try:
                if self.is_element_present(By.CSS_SELECTOR, "#chk-as-gst-btn > span.btn-content"): break
            except:
                pass
            time.sleep(1)
        else:
            self.fail("time out")
        driver.find_element_by_id("chk-as-gst-btn").click()
        driver.find_element_by_id("shipaddress-fullname").clear()
        driver.find_element_by_id("shipaddress-fullname").send_keys("manoj")
        driver.find_element_by_id("shipaddress-addr1").clear()
        driver.find_element_by_id("shipaddress-addr1").send_keys("909 Asbury drive")
        driver.find_element_by_id("shipaddress-city").clear()
        driver.find_element_by_id("shipaddress-city").send_keys("Buffalogrove`")
        driver.find_element_by_id("shipaddress-city").clear()
        driver.find_element_by_id("shipaddress-city").send_keys("Buffalogrove")
        Select(driver.find_element_by_id("shipaddress-state")).select_by_visible_text("Illinois")
        driver.find_element_by_id("shipaddress-zip").clear()
        driver.find_element_by_id("shipaddress-zip").send_keys("60089")
        driver.find_element_by_id("shipaddress-phone").click()
        driver.find_element_by_id("shipaddress-phone").clear()
        driver.find_element_by_id("shipaddress-phone").send_keys("9999999999")
        driver.find_element_by_link_text("PayPal").click()
        driver.find_element_by_link_text("Credit / Debit Card").click()
        driver.find_element_by_id("ccnumber").clear()
        driver.find_element_by_id("ccnumber").send_keys("4111111111111111111111111111111")
        driver.find_element_by_id("expmonth").click()
        try:
            self.assertEqual("4. Billing Address",
                             driver.find_element_by_xpath("//div[@id='wizard-step-content']/article[5]/h3").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("5. Enter Email Address",
                             driver.find_element_by_xpath("//div[@id='wizard-step-content']/article[7]/h3").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("3. Payment Method",
                             driver.find_element_by_xpath("//div[@id='wizard-step-content']/article[3]/h3").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("2. Shipping Method",
                             driver.find_element_by_xpath("//div[@id='wizard-step-content']/article[2]/h3").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("1. Shipping Address", driver.find_element_by_css_selector("h3.section-header").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertTrue(self.is_element_present(By.LINK_TEXT, "Edit Order"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("layout").click()
        driver.find_element_by_link_text("Edit Order").click()
        driver.find_element_by_link_text("Remove").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to.alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))
