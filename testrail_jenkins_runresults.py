"""
TestRail integration
"""
# C:\Users\xmxm274\AppData\Local\Programs\Python\Python35-32\python.exe
# C:\Users\xmxm274\PycharmProjects\Testrail_Jenkins\testrail_jenkins_runresults.py %ID%

from xml.dom.minidom import parse  # to parse the report.xml
import os  # to find the file and write back
from os import listdir  # to get all the xml files in the given directory
import sys  # to pass the runID as arguments
import testrail  # Api given by Testrail to read and write test results
import xml.dom.minidom

TESTRAIL_URL = 'https://zoro.testrail.net/'
TESTRAIL_USER = 'manoj.mukka@zoro.com'
TESTRAIL_PASSWORD = 'zorothefox'


# project name in the Testrail
# PROJECT_NAME = 'Zoro.com'  # should be capital 'Z'


# Creating a  Testrail object for the class APIClient form testrail.py and passing login credentials
def create_testrail_object():
    tr_object = testrail.APIClient(TESTRAIL_URL)
    tr_object.user = TESTRAIL_USER
    tr_object.password = TESTRAIL_PASSWORD
    return tr_object


# To get the case_ids and titles of each test case in the specified runID
def get_tests_id(tr_object, run_id):
    test_details = {}
    # using send_get to get the test case details
    id_test_cases = tr_object.send_get('get_tests/{}'.format(run_id))
    for case in id_test_cases:
        test_details[case.get('title')] = {
            'case_id': case.get('case_id')
        }
    return test_details


# To get the corresponding test case id using title
def get_case_id(name, test_dictionary):
    try:
        return test_dictionary[name].get('case_id')
    except:
        return None


def add_test_results(tr_object, result_case_id, status_id, error_message):
    if status_id == 1:
        status_flag = {'status_id': status_id,
                       'comment': "This test worked fine!"}
    if status_id == 5:
        status_flag = {'status_id': status_id,
                       'comment': error_message}
    tr_object.send_post('add_result_for_case/' + str(run_id) + '/' + str(result_case_id), status_flag)


def check_case_name(tr_object, case_name):
    # In 'get_cases/{}&suite_id={}'.format(1, 5) '1' is project id and '4' is suite id for master
    # In 'get_cases/{}&suite_id={}'.format(1, 5) '1' is project id and '5' is suite id for understand_xml
    run_details = tr_object.send_get('/get_run/{}'.format(run_id))
    suite = run_details['suite_id']
    case_ids = tr_object.send_get('get_cases/{}&suite_id={}'.format(1, suite))
    for case in case_ids:
        a = case.get('title')
        if a is case_name:
            return 0
    return 1


def post_test_case(tr_object, case_name):
    # will add the case to section 'New from XML'
    add_case = {
        "title": case_name,
        "type_id": 3,
        "priority_id": 3,
    }
    run_details = tr_object.send_get('/get_run/{}'.format(run_id))
    suite = run_details['suite_id']
    r = tr_object.send_get('get_sections/1&suite_id={}'.format(suite))
    name = 'New testcases'
    for case in r:
        if case['name'] == name:
            section = case['id']
    # tr_object.send_post('add_section/1', {"suite_id": suite, "name": "New from XML"})
    tr_object.send_post('add_case/{}'.format(section), add_case)
    return 1


def add_test_case(tr_object, title):
    case_name = title
    flag = check_case_name(tr_object, case_name)
    if flag is 0:
        print('Test case already exists')
        return None
    if flag is 1:
        posted = post_test_case(tr_object, case_name)
        if posted == 1:
            id_test_cases = tr_object.send_get('get_tests/{}'.format(run_id))
            for case in id_test_cases:
                if case_name == case.get('title'):
                    case_id = case.get('case_id')
                    return case_id


def get_result_id(tr_object, test_ids, trace_list):
    for trace in trace_list:
        trace.normalize()
        title = None
        if trace.hasAttribute("name"):
            title = trace.getAttribute("name")
            print(title)
            if ':' in title:
                title = title.split(':')[1].lstrip()
            if 'test_' in title:
                title = title.split('_')[1].lstrip()
        if title is not None:
            result_case_id = get_case_id(title, test_ids)
            if result_case_id is not None:
                print(result_case_id)
            else:
                print("Test case not found")
                print(title)
                flag1 = add_test_case(tr_object, title)
                if flag1 is None:
                    result_case_id = None
                if flag1 is not None:
                    result_case_id = flag1
                    print(result_case_id)
        if result_case_id is not None:
            error = trace.getElementsByTagName('error')
            if error:
                status_id = 5
                # http://stackoverflow.com/questions/11772038/python-minidom-list-childnode-attributes-per-parent-tag
                for child in error:
                    error_message = child.getAttribute('message')
                print(status_id)
            else:
                status_id = 1
                error_message = None
                print(status_id)
            if status_id is not None:
                add_test_results(tr_object, result_case_id, status_id, error_message)


def main(run_id):
    tr_object = create_testrail_object()
    # run_details = tr_object.send_get('/get_run/{}'.format(run_id))
    # suite = run_details['suite_id']
    # print(suite)
    # r = tr_object.send_get('get_sections/1&suite_id={}'.format(suite))
    # print(r)
    # for case in r:
    #     if case['name'] == 'New from XML':
    #         section = case['id']
    #         print(section)
    test_ids = get_tests_id(tr_object, run_id)
    report_path = "target/test-reports/"
    present_dir = os.getcwd()
    r_path = os.path.join(present_dir, report_path)
    for f in listdir(r_path):
        report_name = os.path.join(r_path, f)
        print(report_name)
        dom_tree = xml.dom.minidom.parse(os.path.join(r_path, f))
        collection = dom_tree.documentElement
        trace_list = collection.getElementsByTagName("testcase")
        get_result_id(tr_object, test_ids, trace_list)


if __name__ == "__main__":
    temp = sys.argv[1]
    print(type(temp), temp)
    run_id = int(temp)
    print(run_id)
    main(run_id)
