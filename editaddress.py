# -*- coding: utf-8 -*-
from selenium import webdriver
import xmlrunner
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time

class Editaddress(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_editaddresscheckoutpage(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search-input").click()
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("drill")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_link_text("Cordless Rotary Hammer Drill, 18V").click()
        driver.find_element_by_id("add-to-cart-btn-G5036796").click()
        driver.find_element_by_css_selector("span.shopping-cart-icon").click()
        driver.find_element_by_css_selector("span.ns-shopping-cart-icon").click()
        driver.find_element_by_id("login-email").clear()
        driver.find_element_by_id("login-email").send_keys("testingzoro@gmail.com")
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("zorothefox")
        driver.find_element_by_xpath("//button[@type='submit']").click()
        for i in range(60):
            try:
                if self.is_element_present(By.LINK_TEXT, "Edit Address"):
                    driver.find_element_by_link_text("Edit Address").click()
                    time.sleep(10)
                    break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        driver.find_element_by_id("in-modal-fullname").clear()
        driver.find_element_by_id("in-modal-fullname").send_keys("")
        driver.find_element_by_id("in-modal-fullname").clear()
        driver.find_element_by_id("in-modal-fullname").send_keys("testing")
        driver.find_element_by_id("in-modal-company").clear()
        driver.find_element_by_id("in-modal-company").send_keys("")
        driver.find_element_by_id("in-modal-addr1").clear()
        driver.find_element_by_id("in-modal-addr1").send_keys("909 Asbury Dr")
        driver.find_element_by_id("in-modal-addr2").clear()
        driver.find_element_by_id("in-modal-addr2").send_keys("Suite 1")
        driver.find_element_by_id("in-modal-city").clear()
        driver.find_element_by_id("in-modal-city").send_keys("Buffalogrove")
        driver.find_element_by_id("in-modal-zip").clear()
        driver.find_element_by_id("in-modal-zip").send_keys("60089")
        driver.find_element_by_id("in-modal-phone").clear()
        driver.find_element_by_id("in-modal-phone").send_keys("9999999999")
        driver.find_element_by_css_selector("div.modal-footer > button.btn.btn-primary").click()
        try:
            self.assertEqual("909 Asbury Dr",
                             driver.find_element_by_css_selector("span.address-line.address-addr1").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("Suite 1", driver.find_element_by_css_selector("span.address-line.address-addr2").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("60089", driver.find_element_by_css_selector("span.address-zip").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("United States",
                             driver.find_element_by_css_selector("span.address-line.address-country").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("9999999999", driver.find_element_by_css_selector("span.address-line.address-phone").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Edit Order").click()
        driver.find_element_by_link_text("Remove").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to.alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))