# -*- coding: utf-8 -*-
from selenium import webdriver
import xmlrunner
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re


class Quickorder(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_quickorder_c(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("My account").click()
        driver.find_element_by_link_text("Quick order").click()
        driver.find_element_by_id("zorocode0").clear()
        driver.find_element_by_id("zorocode0").send_keys("G5180341")
        driver.find_element_by_id("qty0").clear()
        driver.find_element_by_id("qty0").send_keys("1")
        driver.find_element_by_id("zorocode1").clear()
        driver.find_element_by_id("zorocode1").send_keys("G5157311")
        driver.find_element_by_id("qty1").clear()
        driver.find_element_by_id("qty1").send_keys("1")
        driver.find_element_by_id("zorocode2").clear()
        driver.find_element_by_id("zorocode2").send_keys("G1283204")
        driver.find_element_by_id("qty2").clear()
        driver.find_element_by_id("qty2").send_keys("1")
        self.assertRegexpMatches(self.close_alert_and_get_its_text(), r"^Are you sure[\s\S]$")
        driver.find_element_by_id("zorocode1").clear()
        driver.find_element_by_id("zorocode1").send_keys("G1283204")
        self.assertEqual("ZORO# G1283204 has already been input.", self.close_alert_and_get_its_text())
        driver.find_element_by_id("zorocode1").clear()
        driver.find_element_by_id("zorocode1").send_keys("G3007313")
        driver.find_element_by_id("qty1").clear()
        driver.find_element_by_id("qty1").send_keys("1")
        driver.find_element_by_id("zorocode3").clear()
        driver.find_element_by_id("zorocode3").send_keys("1234")
        try:
            self.assertEqual("item not found...",
                             driver.find_element_by_css_selector("tr.zoro_recode3 > td.items").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("0", driver.find_element_by_id("cart-item-count").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("add_cart_button").click()
        driver.find_element_by_id("zoro-logo").click()
        try:
            self.assertEqual("3", driver.find_element_by_id("cart-item-count").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("cart-menu-item").click()
        try:
            self.assertEqual("Items in Your Cart (3 items)", driver.find_element_by_id("product-count-header").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Remove").click()
        driver.find_element_by_link_text("Remove").click()
        driver.find_element_by_link_text("Remove").click()

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to.alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))
