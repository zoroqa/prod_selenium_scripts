# -*- coding: utf-8 -*-
from selenium import webdriver
import xmlrunner
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re


class Multipleof6(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_multipleof6_c(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("G2778408")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_id("qty-G2778408").clear()
        driver.find_element_by_id("qty-G2778408").send_keys("1")
        driver.find_element_by_id("add-to-cart-btn-G2778408").click()
        try:
            self.assertEqual("Must be a multiple of 6", driver.find_element_by_css_selector("div.tooltip-inner").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("qty-G2778408").clear()
        driver.find_element_by_id("qty-G2778408").send_keys("5")
        driver.find_element_by_id("add-to-cart-btn-G2778408").click()
        try:
            self.assertEqual("Must be a multiple of 6", driver.find_element_by_css_selector("div.tooltip-inner").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("qty-G2778408").clear()
        driver.find_element_by_id("qty-G2778408").send_keys("999")
        driver.find_element_by_id("add-to-cart-btn-G2778408").click()
        try:
            self.assertEqual("Must be a multiple of 6", driver.find_element_by_css_selector("div.tooltip-inner").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("qty-G2778408").clear()
        driver.find_element_by_id("qty-G2778408").send_keys("6")
        driver.find_element_by_id("add-to-cart-btn-G2778408").click()
        driver.find_element_by_id("zoro-logo").click()
        driver.find_element_by_id("cart-menu-item").click()
        try:
            self.assertEqual("Items in Your Cart (6 items)", driver.find_element_by_id("product-count-header").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Remove").click()

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to.alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))
