# -*- coding: utf-8 -*-
from selenium import webdriver
import xmlrunner
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class Qasearch(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_zmail(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("electric")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        self.driver.implicitly_wait(30)
        driver.find_element_by_css_selector("i..filter-checkbox-icon").click()
        try: self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "span.filter > strong"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Elctricl Tape, 7 mil, 3/4\"x60 ft., Blk, PK10").click()
        for i in range(60):
            try:
                if "Zoro Customers Also Bought" == driver.find_element_by_css_selector("div.panel-heading").text: break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        driver.find_element_by_id("sidebar-left-banner-image").click()
        driver.find_element_by_id("first-name").clear()
        driver.find_element_by_id("first-name").send_keys("manoj")
        driver.find_element_by_id("last-name").clear()
        driver.find_element_by_id("last-name").send_keys("manoj")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("x@x.com")
        driver.find_element_by_name("btnSubmit").click()
        try: self.assertEqual("", driver.find_element_by_css_selector("img[alt=\"//d1i1j9h9jcbcvu.cloudfront.net/img/zmail/zmail-thank-you-141009.8e08c3e8.jpg\"]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to.alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))
