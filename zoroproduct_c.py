# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest


class Zoroproduct(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_zoroproduct_c(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("zoro")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_link_text("Safety Glasses, Clear, Scratch Resistant").click()
        driver.find_element_by_id("add-to-cart-btn-G4068891").click()
        driver.find_element_by_css_selector("i.fa.fa-shopping-cart").click()
        driver.find_element_by_link_text("Remove").click()
        driver.find_element_by_css_selector("img[alt=\"Zoro\"]").click()
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("zoro")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_xpath(
            "//a[contains(text(),'Zoro Red,  Cotton 12\" x 14\" Shop Towel,  Pack of 25')]").click()
        driver.find_element_by_id("add-to-cart-btn-G1428768").click()
        driver.find_element_by_css_selector("span.shopping-cart-icon").click()
        driver.find_element_by_link_text("Remove").click()
        driver.find_element_by_css_selector("img[alt=\"Zoro\"]").click()
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("zoro")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_link_text("ZORO 25 ft. 12/3 SJTW Lighted Extension Cord YL/BL").click()
        driver.find_element_by_id("add-to-cart-btn-G1385983").click()
        driver.find_element_by_css_selector("span.shopping-cart-icon").click()
        driver.find_element_by_link_text("Remove").click()
        driver.find_element_by_css_selector("img[alt=\"Zoro\"]").click()
        driver.find_element_by_id("search-input").click()
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("zoro")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_xpath("//a[contains(text(),'Pro Mechanics Gloves,  L')]").click()
        driver.find_element_by_id("add-to-cart-btn-G1457108").click()
        driver.find_element_by_css_selector("span.shopping-cart-icon").click()
        driver.find_element_by_link_text("Remove").click()
        driver.find_element_by_css_selector("img[alt=\"Zoro\"]").click()
        driver.find_element_by_id("search-input").click()
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("zoro")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_link_text("ZORO 25 ft. 14/3 SJTW Lighted Extension Cord OR/BL").click()
        driver.find_element_by_id("add-to-cart-btn-G1385987").click()
        driver.find_element_by_css_selector("span.shopping-cart-icon").click()
        driver.find_element_by_link_text("Remove").click()

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to.alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
