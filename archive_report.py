import os  # to find the file and write back
import shutil  # shutil.move is used to move the file after it is done processing
from os import listdir  # to get all the xml files in the given directory


def main():
    report_path = "target/test-reports/"
    archive_path = "target/archive"
    present_dir = os.getcwd()
    r_path = os.path.join(present_dir, report_path)
    a_path = os.path.join(present_dir, archive_path)
    for f in listdir(r_path):
        report_name = os.path.join(r_path, f)
        print(report_name)
        if os.path.exists(a_path):
            shutil.move(os.path.join(r_path, f), os.path.join(a_path, f))
            print('The file " ' + f + ' " is moved to archive folder')
        if not os.path.exists(a_path):
            os.makedirs(a_path)
            shutil.move(os.path.join(r_path, f), os.path.join(a_path, f))
            print('The file' + f + 'is moved to archive folder')



if __name__ == "__main__": main()
