# -*- coding: utf-8 -*-
from selenium import webdriver
import xmlrunner
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest

class Signedincheckout(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_signedincheckout(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("gear")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_link_text("Gear, Spur, 12 Pitch").click()
        driver.find_element_by_id("add-to-cart-btn-G2264202").click()
        driver.find_element_by_css_selector("span.shopping-cart-icon").click()
        driver.find_element_by_css_selector("span.ns-shopping-cart-icon").click()
        driver.find_element_by_id("login-email").clear()
        driver.find_element_by_id("login-email").send_keys("testingzoro@gmail.com")
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("zorothefox")
        driver.find_element_by_xpath("//button[@type='submit']").click()
        try: self.assertEqual("1. Shipping Address", driver.find_element_by_css_selector("h3.section-header").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("2. Shipping Method", driver.find_element_by_xpath("//div[@id='wizard-step-content']/article[2]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("9999999999", driver.find_element_by_css_selector("span.address-line.address-addr2").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("3. Payment Method", driver.find_element_by_xpath("//div[@id='wizard-step-content']/article[3]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='creditcard-module-list-placeholder']/div/div/article/button"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "button.btn.btn-link"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.LINK_TEXT, "Edit Card"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Add New Credit Card", driver.find_element_by_link_text("Add New Credit Card").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.LINK_TEXT, "Add New Credit Card"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='wizard-step-content']/article[4]/h3"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Enter a Gift Certificate Number", driver.find_element_by_css_selector("label.strong").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("4. Billing Address", driver.find_element_by_xpath("//div[@id='wizard-step-content']/article[5]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='place-order-button']/button"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Edit Order").click()
        driver.find_element_by_link_text("Remove").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))
