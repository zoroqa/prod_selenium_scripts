# -*- coding: utf-8 -*-
from selenium import webdriver
import xmlrunner
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re


class Quantity(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.PhantomJS()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_quantity(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("wrench")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        driver.find_element_by_link_text("Pipe Wrench, 18\" L, Aluminum").click()
        driver.find_element_by_id("qty-G1458816").clear()
        driver.find_element_by_id("qty-G1458816").send_keys("9999")
        driver.find_element_by_id("add-to-cart-btn-G1458816").click()
        driver.find_element_by_id("add-to-cart-btn-G1458816").click()
        try:
            self.assertEqual("Must be between 1 and 999", driver.find_element_by_css_selector("div.tooltip-inner").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("qty-G1458816").clear()
        driver.find_element_by_id("qty-G1458816").send_keys("0")
        driver.find_element_by_id("add-to-cart-btn-G1458816").click()
        try:
            self.assertEqual("Must be between 1 and 999", driver.find_element_by_css_selector("div.tooltip-inner").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("qty-G1458816").clear()
        driver.find_element_by_id("qty-G1458816").send_keys("-1")
        driver.find_element_by_id("add-to-cart-btn-G1458816").click()
        try:
            self.assertEqual("Must be between 1 and 999", driver.find_element_by_css_selector("div.tooltip-inner").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("qty-G1458816").clear()
        driver.find_element_by_id("qty-G1458816").send_keys("1111")
        driver.find_element_by_id("add-to-cart-btn-G1458816").click()
        try:
            self.assertEqual("Must be between 1 and 999", driver.find_element_by_css_selector("div.tooltip-inner").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("qty-G1458816").clear()
        driver.find_element_by_id("qty-G1458816").send_keys("")
        driver.find_element_by_id("qty-G1458816").clear()
        driver.find_element_by_id("qty-G1458816").send_keys("15")
        driver.find_element_by_id("add-to-cart-btn-G1458816").click()
        driver.find_element_by_id("zoro-logo").click()
        driver.find_element_by_id("cart-menu-item").click()
        driver.find_element_by_link_text("Remove").click()
        driver.find_element_by_css_selector("img[alt=\"Zoro\"]").click()

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))
