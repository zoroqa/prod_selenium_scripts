# -*- coding: utf-8 -*-
import xmlrunner
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest


class VerifyHomepageLinks(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_homepagelinks(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Sign In").click()
        driver.find_element_by_id("login-radio-new").click()
        driver.find_element_by_id("login-radio-existing").click()
        driver.find_element_by_css_selector("img[alt=\"Zoro\"]").click()
        driver.find_element_by_link_text("My account").click()
        driver.find_element_by_link_text("Quick order").click()
        driver.find_element_by_id("zorocode0").clear()
        driver.find_element_by_id("zorocode0").send_keys("1")
        driver.find_element_by_id("zoro-logo").click()
        driver.find_element_by_link_text("About Us").click()
        try:
            self.assertEqual("About us", driver.find_element_by_css_selector("h1").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("zoro-logo").click()
        driver.find_element_by_link_text("Net 30 terms").click()
        try:
            self.assertEqual("Open Account Billing",
                             driver.find_element_by_xpath("//body[@id='page_zoro_info']/div/div/div/h3[2]").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_id("zoro-logo").click()
        driver.find_element_by_link_text("Resource den").click()
        driver.find_element_by_xpath("//body[@id='page_resource_den']/div/div[2]/div/a/img").click()
        driver.find_element_by_id("zoro-logo").click()
        driver.find_element_by_link_text("Help").click()

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))
