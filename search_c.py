# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time
import xmlrunner

class Addtocart(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_searchpage_c(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("wrench")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        self.driver.implicitly_wait(30)
        driver.find_element_by_xpath("//li[@id='list-toggle']/i").click()
        driver.find_element_by_xpath("//li[@id='grid-toggle']/i").click()
        self.driver.implicitly_wait(30)
        driver.find_element_by_css_selector("button.slick-next.slick-arrow").click()
        driver.find_element_by_css_selector("button.slick-prev.slick-arrow").click()
        Select(driver.find_element_by_css_selector("select.sort_filter")).select_by_visible_text("Price: Low to High")
        driver.find_element_by_xpath("//section[@id='part_content']/div[3]/div[3]/div[2]/div[3]/span/select/option[2]").click()
        Select(driver.find_element_by_css_selector("select.sort_filter")).select_by_visible_text("Price: High to Low")
        driver.find_element_by_xpath("//section[@id='part_content']/div[3]/div[3]/div[2]/div[3]/span/select/option[3]").click()
        Select(driver.find_element_by_css_selector("select.sort_filter")).select_by_visible_text("Price: Low to High")
        driver.find_element_by_xpath("//section[@id='part_content']/div[3]/div[3]/div[2]/div[3]/span/select/option[2]").click()
        driver.find_element_by_link_text("Socket Clip, 3/8 in. Dr").click()
        for i in range(60):
            try:
                if self.is_element_present(By.ID, "add-to-cart-btn-G4745675"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        driver.find_element_by_id("add-to-cart-btn-G4745675").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))