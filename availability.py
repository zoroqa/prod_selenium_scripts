# -*- coding: utf-8 -*-
from selenium import webdriver
import xmlrunner
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time

class Availability(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.zoro.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_availability(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("G1931483")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        for i in range(60):
            try:
                if self.is_element_present(By.CSS_SELECTOR, "span.avl-in-stock"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        driver.find_element_by_id("add-to-cart-btn-G1931483").click()
        driver.find_element_by_css_selector("span.shopping-cart-icon").click()
        driver.find_element_by_css_selector("img[alt=\"Zoro\"]").click()
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("G5036796")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        for i in range(60):
            try:
                if self.is_element_present(By.CSS_SELECTOR, "span.avl-backordered-button"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        driver.find_element_by_id("add-to-cart-btn-G5036796").click()
        driver.find_element_by_css_selector("i.fa.fa-shopping-cart").click()
        driver.find_element_by_css_selector("img[alt=\"Zoro\"]").click()
        driver.find_element_by_id("search-input").clear()
        driver.find_element_by_id("search-input").send_keys("G2778408")
        driver.find_element_by_css_selector("button.btn.btn-orange").click()
        try: self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "span.avl-drop-ship"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        for i in range(60):
            try:
                if self.is_element_present(By.XPATH, "//a[@id='avl-info-icon']/span/i"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        driver.find_element_by_id("add-to-cart-btn-G2778408").click()
        driver.find_element_by_css_selector("i.fa.fa-shopping-cart").click()
        driver.find_element_by_link_text("Remove").click()
        driver.find_element_by_link_text("Remove").click()
        driver.find_element_by_link_text("Remove").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to.alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='target/test-reports'))
